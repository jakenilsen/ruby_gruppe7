require 'test_helper'

class MessagesControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  setup do
    @messages = messages(:one)
    @update ={
        title: 'Lorem Ipsum',
        text: 'Lorem foo Ipsum bar'
    }
  end

  test "should get index" do
    get messages_url
    assert_response :success
  end

  test "should get new" do
    get new_message_url
    assert_response :success
  end

  test "should create message" do
    assert_difference('Message.count') do
      post messages_url, params: { message: { title: @messages.title, text: @messages.text } }
    end

    assert_redirected_to message_url(Message.last)
  end

  test "should show message" do
    get message_url(@messages)
    assert_response :success
  end

  test "should get edit" do
    get edit_message_url(@messages)
    assert_response :success
  end

  test "should update message" do
    patch message_url(@messages), params: { message: { title: @messages.title, text: @messages.text } }
    assert_redirected_to message_url(@messages)
  end

end
