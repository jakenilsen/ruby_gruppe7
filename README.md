# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

We want to build a web application for a group or family, 
it consists of a message board and a to-do list/shopping list. 
Group leaders/parents will be able to modify the message board and list. 
While other group members should only see the message board and comment 
the announcements and mark items they have done on the list.
There will be a login for both type of users for safety concerns.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

bundle install,
rails db:migrate,
rails server

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact