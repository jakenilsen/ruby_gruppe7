class TodoitemsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_todolist
  before_action :set_todoitem, except: [:create]

  def create
    @todoitem = @todolist.todoitems.create(todoitem_params)

    redirect_to @todolist
  end

  def destroy
    if @todoitem.destroy
      flash[:success] = "Task was deleted"
    else
      flash[:error] = "Task could not be deleted"
    end
    redirect_to @todolist
  end

  def complete
    @todoitem.update_attribute(:completedtime, Time.now)

    redirect_to @todolist, notice: "Todo item completed";
  end

  private

  def set_todolist
    @todolist = Todolist.find(params[:todolist_id])
  end

  def set_todoitem
    @todoitem = @todolist.todoitems.find(params[:id])
  end

  def todoitem_params
    params[:todoitem].permit(:content)
  end

end
