class Todoitem < ApplicationRecord
  belongs_to :todolist

  def completed?
    !completedtime.blank?
  end
end
